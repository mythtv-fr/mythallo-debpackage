#!/usr/bin/python
# -*- coding: utf-8 -*-
# Name: mythallo.py
# Python Script
# Author:
# Purpose:
#   This python script is intended to perform Movie data lookups
#   based on information found at allocine.fr website using http://wiki.gromez.fr/dev/api/allocine API.
#   It follows mythtv universal metadata format: http://www.mythtv.org/wiki/MythTV_Universal_Metadata_Format.
# Command example:
# See help (-u and -h) options
#
# License:Creative Commons GNU GPL v2
# (http://creativecommons.org/licenses/GPL/2.0/)
#-------------------------------------
__title__   ="MythAllo"
__author__  ="le.chacal@windowslive.com"
__version__ ="2.3"

# 1.0 Initilal release
# 1.1 debug version
# 1.2 Fixed description in MovieList. Walkarround for repeated identical movie output in MovieList
# 1.3 Cleanup
# 2.0 Change version number. Serie 2.x is for mythtv 0.24 and greater, serie 1.x for mythtv 0.23 and lower
# 2.1 Add "Parental guidance", fix movie rating scaling from 0-5 to 0-10, xmlns="http://www.allocine.net/v6/ns/", code refactoring
# 2.2 Fix crash when no trailer found.
# 2.3 Update to API AlloCiné v3 and switch to json for decoding

from optparse import OptionParser
from lxml import etree as etree
import logging, urllib2, sys, re, json

QUERY_URL      = 'http://api.allocine.fr/rest/v3/search?q=%s&partner=YW5kcm9pZC12M3M&profile=large&format=json'
METADATA_URL   = 'http://api.allocine.fr/rest/v3/movie?partner=YW5kcm9pZC12M3M&profile=large&code=%s&format=json'
TRAILER_URL    = 'http://www.allocine.fr/skin/video/AcVisionData_xml.asp?media=%s'
PEOPLE_URL     = 'http://www.allocine.fr/personne/fichepersonne_gen_cpersonne=%s.html'
MYTHALLO_TAG   = 'myal'
TRAILER_REG_EXP= """hd_path=\"(?P<Link>.+?\.flv)\""""
# create logger
logger = logging.getLogger('mythallo')

class DataGrabber:
    @staticmethod
    def get_json(url):
        logger.debug('Query url: %s\n' % url)
        try:
            json_data = json.load(urllib2.urlopen(url))
        except etree.ParseError:
            json_data = None
        return json_data

#    @staticmethod
#    def get_tree(url, encoding):
#        logger.debug('Query url: %s\n' % url)
#        parser = etree.XMLParser(encoding=encoding)
#        try:
#            xml_tree = etree.parse(urllib2.urlopen(url), parser)
#        except etree.ParseError:
#            xml_tree = None
#        return xml_tree

    @staticmethod
    def get_metadata_json(movie_id):
        url = METADATA_URL % urllib2.quote(movie_id)
        return DataGrabber.get_json(url)

#    @staticmethod
#    def get_metadata_tree(movie_id):
#        url = METADATA_URL % urllib2.quote(movie_id)
#        return DataGrabber.get_tree(url, None)

    @staticmethod
    def get_movies_json(title):
        url = QUERY_URL % urllib2.quote(title.encode('utf-8'))
        return DataGrabber.get_json(url)

#    @staticmethod
#    def get_movies_tree(title):
#        url = QUERY_URL % urllib2.quote(title)
#        return DataGrabber.get_tree(url, None)

#    @staticmethod
#    def get_trailer_tree(trailer_id):
#        url = TRAILER_URL % urllib2.quote(trailer_id)
#        return DataGrabber.get_tree(url, 'ISO-8859-1')

    @staticmethod
    def get_trailer_url(trailer_id):
        url = TRAILER_URL % urllib2.quote(trailer_id)
        try :
            u = urllib2.urlopen(url)
        except IOError :
            logger.debug("Cannot open url: %s" % url)
            data = None
        data = u.read()
        u.close()
        match = re.findall(TRAILER_REG_EXP, data)
        try :
            trailer_url = match[0]
        except IndexError:
            logger.debug("No trailer found for trailer id %s:" % trailer_id)
            trailer_url =''
        return trailer_url

    # get the orignal movie id
    @staticmethod
    def get_allocine_id(mythallo_id):
        allocine_id = re.sub('''^%s'''% MYTHALLO_TAG, '', mythallo_id)
        return allocine_id

    # add a custom tag to allocine id in order to avoid other grabbers to mixup metadata.
    @staticmethod
    def get_mythallo_id(allocine_id):
        mythallo_id = '%s%s' % (MYTHALLO_TAG, allocine_id)
        return mythallo_id


class MythtvXML:
    def __init__(self):
        pass

    def set_values(self, alloncine_tree):
        pass

    def dict_key_val(self, dictionnary, key):
        try:
            value = dictionnary[key]
        except KeyError:
            value = ""
            logger.debug("key: %s not found in dictionnary: %s" % (key, dictionnary))
        return value

    def get_first_element_content(self, elements):
        try:
            text = elements[0].text
        except IndexError:
            text = ''
            logger.debug("No text to retrive \n")
        return text

    def get_first_element_attrib_value(self, elements, attribute):
        try:
            attrib_val = elements[0].get(attribute)
        except IndexError:
            attrib_val = ''
            logger.debug("No attribute to retrive in \n")
        return attrib_val

    def print_mythtv_tree(self):
        sys.stdout.write(etree.tostring(self._metadata, encoding='UTF-8',xml_declaration=True, pretty_print=True))

class MovieList(MythtvXML):
    def __init__(self, movie_title = None):
        self._metadata = etree.Element("metadata")
        if movie_title:
            allocine_list = DataGrabber.get_movies_json(movie_title)
            self.set_values(allocine_list)

    def set_values(self, allocine_list):
        movies = allocine_list["feed"]["movie"]
        try:
            id_set = set()
            for movie in movies:
                movie_id = movie["code"]
                if movie_id not in id_set:
                    # Some query (for exemple 'à l'origine') output strange results.
                    # Some movies are repeated multiple time.
                    # Make sure each movie is outputed only once.
                    self.add_item(movie)
                    id_set.add(movie_id)
        except IndexError:
            logger.debug('No movie found \n')

    def add_item(self, movie):
        ## mythtv metadata movie list structure
        item            = etree.SubElement(self._metadata, "item")
        title           = etree.SubElement(item, "title")
        imdb            = etree.SubElement(item, "imdb")
        language        = etree.SubElement(item, "language")
        subtitle        = etree.SubElement(item, "subtitle")
        user_rating     = etree.SubElement(item, "userrating")
        inetref         = etree.SubElement(item, "inetref")
        description     = etree.SubElement(item, "description")
        release_date    = etree.SubElement(item, "releasedate")

        ## set title & subtitle
        try:
            movie_title = title.text = movie["title"]
            subtitle.text = movie["originalTitle"]
        except KeyError :
            movie_title = title.text = movie["originalTitle"]
            subtitle.text = ''
            logger.debug('Only orginalTitle found for: %s' % movie_title)

        ## imdb, actually no link between imdb and allocine
        imdb.text = 'NA'

        ## set language, obvouisly alloncine is only in french
        language.text = 'fr'

        try:
        ## Ranking range on allocine is between 0-5, scaling up to 0-10
            rating = movie["statistics"]["userRating"]
            user_rating.text = "%0.1f" % (float(rating) * 2)
        except (KeyError, ValueError):
            logger.debug('No user rating found for %s' % movie_title)

        inetref.text = DataGrabber().get_mythallo_id(movie["code"])

        allocine_metadata = DataGrabber.get_metadata_json(str(movie["code"]))
        movie = allocine_metadata["movie"]
        try:
            description.text = movie["synopsisShort"]
        except KeyError:
            logger.debug("No synopsisShort found for: %s" % movie_title)
        try:
            release_date.text = movie["release"]["releaseDate"]
        except KeyError:
            logger.debug("No releaseDate found for: %s" % movie_title)

class MovieData(MythtvXML):
    def __init__(self, movie_id = None):
        self._metadata       = etree.Element("metadata")

        self._item           = etree.SubElement(self._metadata, "item")

        self._title          = etree.SubElement(self._item, "title")
        self._tagline        = etree.SubElement(self._item, "tagline")
        self._language       = etree.SubElement(self._item, "language")
        self._description    = etree.SubElement(self._item, "description")
        self._certifications = etree.SubElement(self._item, "certifications")
        self._categories     = etree.SubElement(self._item, "categories")
        self._studios        = etree.SubElement(self._item, "studios")
        self._countries      = etree.SubElement(self._item, "countries")
        ## popularity
        self._user_rating    = etree.SubElement(self._item, "userrating")
        ## budget
        ## revenue
        self._year           = etree.SubElement(self._item, "year")
        self._releasedate    = etree.SubElement(self._item, "releasedate")
        ## last updated
        self._runtime        = etree.SubElement(self._item, "runtime")
        self._inetref        = etree.SubElement(self._item, "inetref")
        ## imdb
        self._homepage       = etree.SubElement(self._item, "homepage")
        self._trailer        = etree.SubElement(self._item, "trailer")
        self._people         = etree.SubElement(self._item, "people")
        self._images         = etree.SubElement(self._item, "images")

        if movie_id :
            allocine_metadata = DataGrabber.get_metadata_json(movie_id)
            self.set_values(allocine_metadata)

    def set_values(self, allocine_metadata):
        movie       = allocine_metadata["movie"]
        movie_id    = movie["code"]

        ## set title
        try:
            self._title.text = movie["title"]
        except KeyError:
            logger.debug("No title found for movie id: %s" % movie_id)


        ## set tagline, using short synopsys
        try:
            self._tagline.text = movie["synopsisShort"]
        except KeyError:
            logger.debug("No tagline found for movie id: %s" % movie_id)

        ## obviously allocine is french only
        self._language.text = 'fr'

        ## set description, using synopsys
        try:
            self._description.text =  movie["synopsis"]
        except KeyError:
            logger.debug("No description found for movie id: %s" % movie_id)

        ## set certifications
        try:
            etree.SubElement(self._certifications, "certification", locale="fr", name=movie["movieCertificate"]["certificate"]["$"])
        except KeyError:
            logger.debug("No certifications found for movie id: %s" % movie_id)

        ## set movie category
        try:
            for category in movie["genre"]:
                etree.SubElement(self._categories, "category", type="genre", name=category["$"])
        except KeyError:
            logger.debug("No categories found for movie id: %s" % movie_id)

        ## set studio
        try:
            etree.SubElement(self._studios, "studio", name = movie["release"]["distributor"]["name"])
        except KeyError:
            logger.debug("No studio found for movie id: %s" % movie_id)

        ## set countries from nationnalites
        try:
            for country in movie["nationality"]:
                etree.SubElement(self._countries, "country", name=country["$"])
        except KeyError:
            logger.debug("No countries found for movie id: %s" % movie_id)

        ## set user rating
        try:
            rating = movie["statistics"]["userRating"]
            ## Ranking range on allocine is between 0-5, scaling up to 0-10
            self._user_rating.text = "%0.1f" % (float(rating) * 2)
        except KeyError:
            logger.debug("No user rating found for movie id: %s" % movie_id)

        ## set year
        try:
            self._year.text = str(movie["productionYear"])
        except KeyError:
            logger.debug("No year found for movie id: %s" % movie_id)

        ## ser release date
        try:
            self._releasedate.text =  movie["release"]["releaseDate"]
        except KeyError:
            logger.debug("No release date found for movie id: %s" % movie_id)

        ## set runtime
        try:
            rt = int(movie["runtime"])
            self._runtime.text = str(int(rt / 60))
        except KeyError:
            logger.debug("No release date found for movie id: %s" % movie_id)

        ## set inetref
        self._inetref.text = DataGrabber.get_mythallo_id(movie_id)

        ## set homepage
        try:
            self._homepage.text = movie["link"][0]["href"]
        except KeyError:
            logger.debug("No homepage found for movie id: %s" % movie_id)

        ## set trailer
        try:
            trailer_id = str(movie["trailer"]["code"])
            self._trailer.text = DataGrabber.get_trailer_url(trailer_id)
        except KeyError:
            logger.debug("No trailer id found for movie id: %s" % movie_id)

        ## set people, director, actor and producer
        try:
            for member in  movie["castMember"]:
                code=member["activity"]["code"]
                character=None
                if code == 8001: #Acteur
                    job='Actor'
                    department='Actors'
                    character=member["role"]
                elif code == 8002: #Réalisateur
                    job='Director'
                    department='Directing'
                elif code == 8029: #Producteur
                    job='Producer'
                    department='Production'
                else:
                    continue
                name=member["person"]["name"]
                try:
                    url=(PEOPLE_URL % member["person"]["code"])
                except KeyError :
                    logger.debug("Cannot find url for this person %s in movie id: %s" % (name, movie_id))
                    url = ""
                try:
                    thumb=member["picture"]["href"]
                except KeyError :
                    logger.debug("No thumb found for person: %s in movie id: %s" % (name, movie_id))
                    thumb = ""

                people = etree.SubElement(self._people, "person", \
                                            name = name, \
                                            job = job, \
                                            department = department, \
                                            url = url, \
                                            thumb = thumb)
                if character :
                    people.set("character", character)
        except KeyError :
                    logger.debug("No cast found in movie id: %s" % movie_id)

        ## set images, Affiche -> coverart and Photos -> fanart
        try:
            for media in movie["media"]:
                code = media["type"]["code"]
                if code == 31001: #Affiche -> coverart
                    art_type = 'coverart'
                elif code == 31006: #Photo -> fanart
                    art_type = 'fanart'
                else:
                    continue
                try :
                    url = media["thumbnail"]["href"]
                except KeyError :
                    logger.debug("No art url found for movie id: %s" % movie_id)
                    url = ""
                etree.SubElement(self._images, "image", type = art_type, url = url)
        except KeyError :
            logger.debug("No media found for movie id: %s" % movie_id)


def main():
    parser = OptionParser(usage=u"%prog usage: mythallo -hduvlMD [parameters]\n <series name or 'series and season number' or 'series and season number and episode number'>\n")

    parser.add_option(   "--verbose", action="store_true", default=False, dest="verbose",
                        help=u"Show debugging info")
    parser.add_option(  "-u", "--usage", action="store_true", default=False, dest="usage",
                        help=u"Display examples for executing the tmdb script")
    parser.add_option(  "-v", "--version", action="store_true", default=False, dest="version",
                        help=u"Display version and author")
    parser.add_option(  "-l", "--language", metavar="LANGUAGE", default=u'fr', dest="language",
                        help=u"Select data that matches the specified language fall back to fr if nothing found (e.g. 'es' Español, 'de' Deutsch ... etc)")
    parser.add_option(  "-M", "--movielist", action="store_true", default=False, dest="movielist",
                        help=u"Get matching Movie list")
    parser.add_option(  "-D", "--moviedata", action="store_true", default=False, dest="moviedata",
                        help=u"Get Movie metadata including graphic URLs")
    parser.add_option(  "-t", "--test", action="store_true", default=False, dest="test",
                        help=u"Test mode, to check for installed dependencies")

    opts, args = parser.parse_args()

    # Process version command line requests
    if opts.version:
        version = etree.XML(u'<grabber></grabber>')
        etree.SubElement(version, "name").text = __title__
        etree.SubElement(version, "author").text = __author__
        etree.SubElement(version, "thumbnail").text = 'mythallo.png'
        etree.SubElement(version, "command").text = 'mythallo.py'
        etree.SubElement(version, "type").text = 'movie'
        etree.SubElement(version, "description").text = 'Search movie and download metadata on allocine'
        etree.SubElement(version, "version").text = __version__
        sys.stdout.write(etree.tostring(version, encoding='UTF-8', pretty_print=True))
        sys.exit(0)

    if opts.test :
        print "Everything appears to be in order"
        sys.exit(0)

    # Make all command line arguments unicode utf8
    for index in range(len(args)):
        args[index] = unicode(args[index], 'utf8')

    if not len(args) >= 1:
        sys.stderr.write("! Error: There must be at least one value for any option.")
        sys.exit(1)

    # Checkout empty arguments
    if args[0] == u'':
        sys.stderr.write("! Error: There must be a non-empty argument, yours is empty.\n")
        sys.exit(1)


    # Process verbose option
    logger = logging.getLogger('mythallo')
    logger.setLevel(logging.INFO)
    console_handler = logging.StreamHandler()
    logger.addHandler(console_handler)

    if opts.verbose:
        logger.setLevel(logging.DEBUG)

    # Process requested option
    if opts.movielist:                  # Movie Search -M
        movie_title = unicode(" ".join(args))
        movie_list = MovieList(movie_title)
        movie_list.print_mythtv_tree()

    elif opts.moviedata:                # Movie metadata -D
        movie_id = DataGrabber().get_allocine_id(args[0])
        metadata = MovieData(movie_id)
        metadata.print_mythtv_tree()

    logger.removeHandler(console_handler)

#    sys.exit(0)
# end main()

if __name__ == '__main__':
    main()
